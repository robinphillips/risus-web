<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require_once('inc_head_php.php');

// Load a list of clichés into a <SELECT>
$ajaxdb = new SQLite3(DBFILE);
$sql = 'SELECT * FROM cliches ' .
	'WHERE cliche_charid = ' . intval($_GET['charid']) .
	' ORDER BY full DESC';
$cliches = $ajaxdb->query($sql);

while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	if ($cliche['current'] < 0)
		$cliche['current'] = 0;
	if ($cliche['doublepump'] == 1)
		$maxpump = $cliche['current'] * 2;
	else
		$maxpump = $cliche['current'];
	
	echo '<option value="' .
		$cliche['clicheid'] . '" ' .
		'data-maxpump="' . $maxpump . '">' .
		htmlentities ($cliche['cliche'], ENT_QUOTES) . ' ' .
		$cliche['current'] . ' ' .
		clichevalue ($cliche['full'], $cliche['doublepump']) . '>' .
		'</option>';
}

$ajaxdb->close();
?>

<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/
require("inc_head_php.php");
require("inc_head_html.php");
?>

<h1>Add Character</h1>

<?php
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Insert main character details
	$sql = "INSERT INTO characters (name, description, notes, npc) VALUES (".
		"'".$db->escapeString($_POST["name"])."',".
		"'".$db->escapeString($_POST["description"])."',".
		"'".$db->escapeString($_POST["notes"])."',";
	if (isset($_POST["npc"]))
		$sql .= "1";
	else
		$sql .= "0";
	$sql .= ")";
	if ($db->exec($sql) === False)
		echo "<p>".$db->lastErrorMsg()."</p>";
	$charid = $db->querySingle("SELECT charid FROM characters WHERE rowid=".$db->lastInsertRowID());

	// Insert clichés
	for ($i=1; $i<=6; $i++) {
		if ($_POST["cliche$i"] != "") {
			if (isset($_POST["doublepump$i"]))
				$doublepump = 1;
			else
				$doublepump = 0;
			$sql = "INSERT INTO cliches (cliche_charid, cliche, full, current, doublepump) VALUES (".
				$charid.",".
				"'".$db->escapeString($_POST["cliche$i"])."',".
				intval($_POST["clichevalue$i"]).",".
				intval($_POST["clichevalue$i"]).",".
				"$doublepump)";
			if ($db->exec($sql) === False)
				echo "<p>".$db->lastErrorMsg()."</p>";
		}
	}
	echo "<p class='good'>Added ".htmlentities($_POST["name"], ENT_QUOTES).".</p>";
}
?>
<script>
$(function() {
	// Alternate between parantheses and square brackets
<?php
for ($i=1; $i<=6; $i++) {
?>
	$("#doublepump<?=$i;?>").change(function(event){
		if ($("#doublepump<?=$i;?>").prop("checked")) {
			$("#open<?=$i;?>").text("[")
			$("#close<?=$i;?>").text("]")
		}
		else {
			$("#open<?=$i;?>").text("(")
			$("#close<?=$i;?>").text(")")
		}
	})
<?php
}
?>
})
</script>

<form method="post">
<div class="box">
<p>Name:<br><input type="text" name="name"><br>
Description:<br><textarea name="description"></textarea><br>
Notes:<br><textarea name="notes"></textarea><br>
<input type="checkbox" id="npc" name="npc"> <label for="npc">Check if this is an NPC</label>
</p>
</div>

<h2>Clichés</h2>

<div class="box">
<p>
<?php
for ($i=1; $i<=6; $i++) {
	echo "<input name='cliche$i' list='datalist_cliches'> ";
	echo "<span id='open$i'>(</span>";
	echo "<input name='clichevalue$i' style='width:4ex;' pattern='[0-9]+'>";
	echo "<span id='close$i'>)</span> ";
	echo "<input type='checkbox' id='doublepump$i' name='doublepump$i'><label for='doublepump$i'> Double-pump cliché</label>";
	echo "<br>";
}
?>
</p>
</div>

<p>
<input type="submit" name="btnSubmit" value="Add character">
</p>
</form>

<?php
require("inc_foot.php");
?>

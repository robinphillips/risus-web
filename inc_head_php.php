<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require ("inc_config.php");
$db = new SQLite3(DBFILE);
$log = "";

require ("inc_auth.php");

// If user is a guest, unset any POST variables to stop them doing any damage, except on login page
if (ROLE == "guest" && basename($_SERVER['PHP_SELF']) != "login.php")
	unset($_POST);
// Redirect if user does not have rights to view this page
if (substr(basename($_SERVER['PHP_SELF']), 0, 3) == "gm_" && ROLE != "gm") {
	// PHP redirection
	header ("Location:".BASEURL);
	// JavaScript redirection
	echo "\n<script>\nlocation.href='".BASEURL."'\n</script>\n";
	// echo link as backup
	echo "\n<p>You do not have permission to access this page. <a href='".BASEURL."'>Go to the home page instead.</a></p>\n";
	// Die, so that nothing else is output
	die();
}

// Links. GM only have "gm_" prefix
$links = array (
	"index.php" => "Characters",
	"combat.php" => "Combat",
	"combat.php?type=sac" => "Single-Action Conflict",
	"tn.php" => "Target Number",
	"team_individual.php" => "Team vs Individual",
	"team_team.php" => "Team vs Team",
	"dicelog.php" => "Dice Log",
	"gm_clicheloan.php" => "Loaning Clichés",
	"gm_addchar.php" => "Add PC/NPC",
	"gm_massedit.php" => "Mass Update PCs/NPCs",
	"gm_editchar.php" => "Edit PC/NPC",
	"gm_editplayer.php" => "Add/Edit Player",
	"diceroller.php" => "Dice Roller",
);
if (ROLE == "guest")
	$links [LOGINURL] = "Log In";
else {
	$links ["profile.php"] = "User Profile";
	$links [LOGOUTURL] = "Log Out";
}

/*
Roll dice
*/
function dice_roll ($dice, $sides=6) {
	// Initialise result array
	$result = array ();
	if ($dice == 0)
		$result[] = 0;
	else
		for ($i = 1; $i <= $dice; $i++) {
			if ($sides == "Average") {
				$roll = mt_rand(1, 6);
				if ($roll == 1)
					$roll = 3;
				if ($roll == 6)
					$roll = 4;
			}
			else
				$roll = mt_rand(1, $sides);
			$result[] = $roll;
		}
	return $result;
}

/*
Log to database and send to XMPP chatroom
*/
function logdb ($str) {
	global $db;

	// Insert log entry into database
	$sql = "INSERT INTO log('log') VALUES ('".$db->escapeString($str)."')";
	$db->exec($sql);

	// Send to XMPP chatroom
	if (USE_XMPP === True) {
		// Turn <BR> tags into newlines
		$arg = str_ireplace(array('<br>', '<br />'), "\n", $str);
		// Remove any remaining tags, convert entities into characters, and escape for use as a shell argument
		$arg = escapeshellarg(html_entity_decode(strip_tags($arg), ENT_QUOTES));
		// Run sendxmpp
		exec ("echo $arg | ".XMPP_EXE." --jserver '".XMPP_SERVER."' --username '".XMPP_USER."' --password '".XMPP_PASSWORD."' --tls --resource '".XMPP_RESOURCE."' --chatroom '".XMPP_CHATROOM."'");
	}
}

/*
Output an <option> list of characters, with character ID $selected selected
$activeonly is 1 if only active characters/NPCs are to be included
Return the ID of the selected character
*/
function selectCharacters($db, $selected, $activeonly = 1) {
	$selectedid = 0;
	if ($activeonly == 1)
		$active = " active = 1 AND";
	else
		$active = "";
	$sql = "SELECT * FROM characters WHERE$active npc = 0 ORDER BY name";
	$pcs = $db->query($sql);
	echo "<optgroup label='Player Characters'>\n";
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC)) {
		// By default, $selectedid is first character listed
		if ($selectedid == 0)
			$selectedid = $pc["charid"];
		echo "<option value='".$pc["charid"]."'";
		if ($pc["charid"] == intval($selected)) {
			$selectedid = $pc["charid"];
			echo " selected";
		}
		echo ">".htmlentities($pc["name"], ENT_QUOTES)."</option>\n";
	}
	echo "</optgroup>\n";

	$sql = "SELECT * FROM characters WHERE$active npc = 1 ORDER BY name";
	$npcs = $db->query($sql);
	
	echo "<optgroup label='NPCs'>\n";
	while ($npc = $npcs->fetchArray(SQLITE3_ASSOC)) {
		echo "<option value='".$npc["charid"]."'";
		if ($npc["charid"] == intval($selected)) {
			$selectedid = $npc["charid"];
			echo " selected";
		}
		echo ">".htmlentities($npc["name"], ENT_QUOTES)."</option>\n";
	}
	echo "</optgroup>\n";
	
	return $selectedid;
}

// Simple function: returns "die" or "dice" (Yes, I'm a pedant)
function die_dice ($d) {
	if ($d == 1)
		return "die";
	else
		return "dice";
}

// Display cliché value, in parentheses or square brackets, as approriate
function clichevalue ($value, $doublepump=0) {
	if ($doublepump == 0)
		$ret = "(".$value.")";
	else
		$ret = "[".$value."]";
	return $ret;
}
?>

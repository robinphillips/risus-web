<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");

$err = "";
$msg = "";
$useremail = "";
$baselink = BASEURL . "login.php";

if (isset($_GET["reset"])) {
	// Get email associated with the reset code
	$sql = "SELECT email FROM players WHERE reset = '".$db->escapeString($_GET["reset"])."'";
	$useremail = $db->querySingle($sql);
}

// Handle logout
if (isset($_GET["action"]) && $_GET["action"] == "logout") {
	// Blank cookies and set expire time to an hour ago so that they are deleted
	foreach ($_COOKIE as $key => $val)
		if (substr($key, 0, 3) == "rw_")
			setcookie ($key, "", time() - 3600);
	// Remove login token from database
	$sql = "UPDATE players SET token = NULL WHERE playerid = ".PLAYERID;
	$db->exec($sql);
	// Go to index page
	header("Location:".BASEURL);
}

if (isset($_POST["btnLogin"]) && $_POST["btnLogin"] != "") {
	if (isset($_POST["reset"]) && $_POST["reset"] != "") {
		// User set new password
		$sql = "UPDATE players
			SET password='".$db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT))."', reset = NULL
			WHERE email LIKE '".$db->escapeString($_POST["email"])."'
			AND reset = '".$db->escapeString($_POST["reset"])."'";
		$db->exec($sql);
		// Reload page without reset code
		header("Location:$baselink");
	}
	else {
		// Normal login. Check password
		$sql = "SELECT password FROM players WHERE email LIKE '".$db->escapeString($_POST["email"])."'";
		$hash = $db->querySingle($sql);

		if (password_verify($_POST["password"], $hash)) {
			// Set a token and put it in a cookie
			$token = bin2hex(openssl_random_pseudo_bytes(12));
			$sql = "UPDATE players
				SET token='".$db->escapeString($token)."'
				WHERE email LIKE '".$db->escapeString($_POST["email"])."'";
			$db->exec($sql);
			if (isset($_POST["stayloggedin"]) && $_POST["stayloggedin"] != "") {
				// Set expire time to 30 days hence
				$expire =  time()+60*60*24*30;
				setcookie ("rw_stay", 1, $expire);
			}
			else {
				// Expire at end of session
				$expire = 0;
				setcookie ("rw_stay", 0);
			}
			// Set login token cookie
			setcookie ("rw_token", $token, $expire);
			// Go to index page
			header("Location:".BASEURL);
		}
		else {
			// Login failed. Make sure token is not set
			$sql = "UPDATE players SET token = NULL WHERE playerid = ".PLAYERID;
			$db->exec($sql);
			$err = "Login failed. Please check your password.";
		}
	}
}

if (isset($_POST["btnReset"]) && $_POST["btnReset"] != "") {
	// Generate random link
	$reset = sha1(openssl_random_pseudo_bytes(64));
	$link = $baselink . "?reset=$reset";
	// Update database with random link. Set password to an invalid value to prevent logins, and token to NULL
	$sql = "UPDATE players 
		SET password='RESET', token = NULL, reset='".$db->escapeString($reset)."'
		WHERE email LIKE '".$db->escapeString($_POST["f_email"])."'";
	if ($db->exec($sql) === False)
		$err = "There was a problem resetting your password. Please check you have entered your email correctly.";
	else {
		// Send link to player
		$msg = "You requested a Risus Web password reset. Click this link to reset your password:\n$link\n\n";
		$msg .= "You will not be able to log in until your password has been reset.\n\n";
		$msg .= "--\nRisus Web\n$baselink\n";
		mail ($_POST["email"], "Risus Web password reset", $msg);
		$msg = "A reset link has been emailed to you.";
	}
}

if ($useremail == "")
	echo "<h1>Log In</h1>\n";
else {
	echo "<h1>Reset Password</h1>\n";
	$msg = "Enter your new password below.";
}

if ($err != "")
	echo "<p class='bad'>$err</p>";
if ($msg != "")
	echo "<p class='good'>$msg</p>";
?>

<form method="post">
<p>
<div class="box">
<p>
Email: 
<?php
if ($useremail == "")
	echo "<input name='email'><br>\n";
else {
	echo "<input type='hidden' name='reset' value='".htmlentities($_GET["reset"], ENT_QUOTES)."'>";
	echo "<input type='hidden' name='email' value='".htmlentities($useremail, ENT_QUOTES)."'>";
	echo "$useremail<br>\n";
}
?>
Password: <input type="password" name="password">
</p>
<p>
<?php
if ($useremail == "") {
	// Log in buttons
	echo "<input type='checkbox' name='stayloggedin' id='stayloggedin'> <label for='stayloggedin'>Stay logged in</label>";
	echo "</p><p>";
	echo "<input type='submit' value='Log in' name='btnLogin'>\n";
}
else {
	// Reset password button
	echo "<input type='submit' value='Set new password' name='btnLogin'>\n";
}
?>
</p>
</div>
</p>
<?php
if ($useremail == "") {
?>
	<p>
	<div class="box">
	<p class = "boxtitle">Forgotten password</p>
	<p>If you have forgotten your password, simply enter your email below and click the button.<br>
	A link will be emailed to you. Click on that link to set a new password.</p>
	<p>
	<input name="f_email">
	</p>
	</p>
	<input type="submit" value="Reset password" name="btnReset">
	</p>
	</div>
	</p>
<?php
}
?>
</form>

<?php
require("inc_foot.php");
?>

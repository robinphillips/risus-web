<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

if (isset($_GET["displaydate"])) {
	$limit = 0;
	$displaydate = $_GET["displaydate"];
	$h = htmlentities($displaydate, ENT_QUOTES);
}
else {
	if (isset($_GET["limit"]))
		$limit = intval($_GET["limit"]);
	else
		$limit = 10;
	$displaydate = "";
	$h = "Last ".number_format($limit)." entries";
}

require("inc_head_php.php");
require("inc_head_html.php");
?>
<script>
$(function() {
	$("#displaydate").datepicker({
		maxDate: "0",
		dateFormat: "yy-mm-dd"
	})
})
</script>

<h1>Dice Log: <?=$h;?></h1>

<form method = "get">
<p>
Display
<select name="limit">
<?php
$options = array (10, 25, 50, 100, 1000);
foreach ($options as $option) {
	echo "<option value='$option'";
	if ($option == $limit)
		echo " selected";
	echo ">last ".number_format($option)." entries";
	echo "</option>";
}
?>
</select>
<input type="submit" value="Go">
</p>
</form>

<form method = "get">
<p>
Or entries from date: <input id="displaydate" name="displaydate" style="width:12ex;">
<input type="submit" value="Go">
</p>
</form>

<?php
if ($limit != "")
	$sql = "SELECT * FROM log ORDER BY datetime DESC LIMIT $limit";
else
	$sql = "SELECT * FROM log WHERE datetime LIKE '$displaydate%' ORDER BY datetime DESC";
$logs = $db->query($sql);

$date = "";
echo "<div style='display:none'>"; // Open an invisible DIV to be closed with first record
while ($log = $logs->fetchArray(SQLITE3_ASSOC)) {
	if ($date != date("jS F Y", strtotime($log["datetime"]))) {
		$date = date("jS F Y", strtotime($log["datetime"]));
		echo "</div>\n\n<div class='alternate'><h2>$date</h2></div>\n";
	}
	echo "<div class='alternate'><h3>" . date("H:i", strtotime($log["datetime"])) . "</h3>\n";
	echo "<p>" . $log["log"] . "</p></div>\n";
}
echo "";

require("inc_foot.php");
?>

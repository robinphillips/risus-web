<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/


/*
Authentication. This file provides built-in authentication against users stored in the SQLite database.
It may be replaced to authenticate against a different database, or to use LDAP, IMAP, etc.

This should define the following constants:
LOGINURL: URL of the login page
LOGOUTURL: URL to log out
ROLE: "player", "gm", or "guest"
PLAYERID: playerid from SQLite
CHARACTERID: ID of player's character in SQLite (or 0 if they don't have a character)
PLAYERNAME: The player's display name
EMAIL: player's email
*/

define ("LOGINURL", "login.php");
define ("LOGOUTURL", "login.php?action=logout");

// If token cookie exists, check against database
if (isset ($_COOKIE["rw_token"])) {
	// Find user
	$sql = "SELECT * FROM players WHERE token = '".$db->escapeString($_COOKIE["rw_token"])."'";
	$p = $db->querySingle($sql, True);

	// Get role (player, guest, gm) and character ID
	if (count($p) > 0) {
		if ($p["gm"] == 1)
			define ("ROLE", "gm");
		else
			define ("ROLE", "player");
		define ("PLAYERID", $p["playerid"]);
		define ("CHARACTERID", $p["player_charid"]);
		define ("PLAYERNAME", $p["name"]);
		define ("EMAIL", $p["email"]);
	}
	else {
		// Token is not in the database. Set as guest
		define ("ROLE", "guest");
	}
}
else {
	// User is not logged in - set as guest
	define ("ROLE", "guest");
}

if (ROLE == "guest") {
	define ("PLAYERID", 0);
	define ("CHARACTERID", 0);
	define ("PLAYERNAME", "Guest");
	define ("EMAIL", "guest@example.com");
}
?>

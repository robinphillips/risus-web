<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

// Base URL, including trailing /
define ("BASEURL", "http://www.example.com/risus/");

// SQLite3 database file
define ("DBFILE", "risus.db");

// XMPP Settings. Set USE_XMPP to True to enable XMPP support
define ("USE_XMPP", False);
// Location of sendxmpp
define ("XMPP_EXE", "/usr/local/bin/sendxmpp");
// XMPP user and password
define ("XMPP_USER", "risus@example.com");
define ("XMPP_PASSWORD", "password");
// XMPP server, resource, chatroom
define ("XMPP_SERVER", "example.com");
define ("XMPP_RESOURCE", "Risus website");
define ("XMPP_CHATROOM", "risuschat@conference.example.com");

// Array of extra links. Key is URL, value is text to display
$extlinks = array (
	"http://www.risusiverse.com/" => "Risusiverse",
	"https://www.drivethrurpg.com/product/170294/?affiliate_id=235519" => "Risus on DriveThruRPG",
);

// Array of GM-only extra links. Key is URL, value is text to display
$gm_extlinks = array (
	"https://rpg.phillipsuk.org/doku.php/risus/" => "Risus on Robin's RPG Resources",
);
?>

<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");

function displayCharacter ($db, $pc) {
	$charid = $pc["charid"];
	echo "<div class='box character' data-charid='$charid'>";

	// Indicate if character has description and/or notes
	if ($pc["description"] != "" && $pc["notes"] != "")
		$suffix = " (D N)";
	elseif ($pc["description"] != "")
		$suffix = " (D)";
	elseif ($pc["notes"] != "")
		$suffix = " (N)";
	else
		$suffix = "";
	
	echo "<p class='boxtitle'>".htmlentities($pc["name"], ENT_QUOTES).$suffix;
	if (ROLE == "gm")
		echo "<a style='font-size:small;float:right;' href='gm_editchar.php?id=$charid'>edit</a>";
	echo "</p>\n";

	if ($pc["description"] != "")
		echo "<p class='hidden' id='d$charid'>" . htmlentities($pc["description"], ENT_QUOTES) . "</p>\n";

	echo "<p class='cliches'>";
	$sql = "SELECT * FROM cliches WHERE cliche_charid = ".$pc["charid"]." ORDER BY full DESC";
	$cliches = $db->query($sql);
	while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC))
		echo htmlentities($cliche["cliche"], ENT_QUOTES) . " ".$cliche["current"]." ".clichevalue($cliche["full"], $cliche["doublepump"])."<br>";
	echo "</p>\n";

	if ($pc["notes"] != "")
		echo "<p class='hidden' id='n$charid'>" . htmlentities($pc["notes"], ENT_QUOTES)."</p>\n";

	echo "</div>\n";
}

function displayCharacters($db, $npc) {
	// Display player's character if they have one
	$sql = "SELECT * FROM characters WHERE active = 1 AND charid = ".CHARACTERID;
	$pc = $db->querySingle($sql, True);
	if (count($pc) > 0)
		displayCharacter($db, $pc);

	// Display other characters
	$sql = "SELECT * FROM characters WHERE npc = $npc AND active = 1 AND charid <> ".CHARACTERID." ORDER BY name";
	$pcs = $db->query($sql);
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC))
		displayCharacter($db, $pc);
}
?>

<script>
$(function() {
	$(".character").click(function() {
		$("#d"+$(this).data("charid")).toggle()
		$("#n"+$(this).data("charid")).toggle()
	})
	
	$("#showAll").click(function() {
		if ($("#showAll").text() == "Show all descriptions and notes") {
			$("#showAll").text("Hide all descriptions and notes")
			$(".hidden").show()
		}
		else {
			$("#showAll").text("Show all descriptions and notes")
			$(".hidden").hide()
		}
	})
})
</script>

<h1>Active Player Characters</h1>

<p>
(<strong>D</strong> indicates that the character has a description. <strong>N</strong> indicates that it has notes). Click on a character to show/hide description and notes. <a href="#" id="showAll">Show all descriptions and notes</a>.
</p>

<?php
displayCharacters($db, 0);

if (ROLE == "gm") {
?>

<h1>Active NPCs</h1>

<?php
displayCharacters($db, 1);
}

require("inc_foot.php");
?>

<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");

if (!isset($_GET["id"])) {
?>
	<h1>Edit Character</h1>

	<p>Choose the character to edit:</p>

	<form method="get">
	<p>
	<select name="id">
<?php
	selectCharacters($db, 0, 0);
?>
	</select>
	<input type="submit" name="btnChoose" value="Edit">
	</p>
	</form>
<?php
}
else {
	$charid = intval($_GET["id"]);

	if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
		// Update characters table
		if (isset($_POST["npc"]))
			$npc = 1;
		else
			$npc = 0;
		if (isset($_POST["active"]))
			$active = 1;
		else
			$active = 0;
		$sql = "UPDATE characters SET ".
			"name='".$db->escapeString($_POST["name"])."', ".
			"description='".$db->escapeString($_POST["description"])."', ".
			"notes='".$db->escapeString($_POST["notes"])."', ".
			"npc=$npc, ".
			"active=$active ".
			"WHERE charid = $charid";
		$db->exec($sql);

		// Update existing clichés
		foreach ($_POST as $key=>$value) {
			if (substr($key, 0, 13) == "clichecurrent" && strlen($key) > 13) {
				$id = substr($key, 13);
				if (isset($_POST["doublepump$id"]))
					$doublepump = 1;
				else
					$doublepump = 0;
				$sql = "UPDATE cliches SET cliche = '".$db->escapeString($_POST["cliche$id"])."',
					current = ".intval($_POST["clichecurrent$id"]).",
					full = ".intval($_POST["clichefull$id"]).",
					doublepump = $doublepump
					WHERE clicheid = $id";
				$db->exec($sql);
			}
		}
		
		// Delete clichés
		foreach ($_POST as $key=>$value) {
			$sql = "";
			if (substr($key, 0, 12) == "clichedelete" && strlen($key) > 12)
				$sql = "DELETE FROM cliches WHERE clicheid = ".substr($key,12)." LIMIT 1";
			$db->exec($sql);
		}
		
		// Add new cliché
		if (isset($_POST["newcliche"]) && $_POST["newcliche"] != "") {
			$sql = "INSERT INTO cliches (cliche_charid, cliche, full, current) VALUES ($charid, '".$db->escapeString($_POST["newcliche"])."', ".intval($_POST["newclichefull"]).", ".intval($_POST["newclichecurrent"]).")";
			$db->exec($sql);
		}
	}

	$sql = "SELECT * FROM characters WHERE charid = $charid";
	$char = $db->querySingle($sql, True);
	
	echo "<h1>Edit Character: ".htmlentities($char["name"], ENT_QUOTES)."</h1>\n";
	if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "")
		echo "<p class='good'>Character updated.</p>";
	?>

	<form method="post">
	<div class="box">
	<p>Name:<br><input type="text" name="name" value="<?=htmlentities($char["name"], ENT_QUOTES);?>"><br>
	Description:<br><textarea name="description"><?=htmlentities($char["description"], ENT_QUOTES);?></textarea><br>
	Notes:<br><textarea name="notes"><?=htmlentities($char["notes"], ENT_QUOTES);?></textarea><br>
	<?php
	if ($char["npc"] == 1)
		$npccheck = " checked";
	else
		$npccheck = "";
	if ($char["active"] == 1)
		$activecheck = " checked";
	else
		$activecheck = "";
	?>
	<input type='checkbox' id='npc' name='npc'<?=$npccheck;?>> <label for="npc">Check if this is an NPC</label><br>
	<input type='checkbox' id='active' name='active'<?=$activecheck;?>> <label for="active">Active character/NPC</label>
	</p>
	</div>

	<br>
	<div class="box">
	<table>
	<tr><th>Cliché</th><th>Current</th><th>Full</th><th>&nbsp;</th><th>&nbsp;</th></tr>
	<?php
	$sql = "SELECT * FROM cliches WHERE cliche_charid = $charid ORDER BY full DESC";
	$cliches = $db->query($sql);

	while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
		$clicheid = $cliche["clicheid"];
		echo "<tr>";
		echo "<td><input name='cliche$clicheid' value='".htmlentities($cliche["cliche"], ENT_QUOTES)."'></td>";
		echo "<td><input name='clichecurrent$clicheid' style='width:4ex;' value='".$cliche["current"]."' required pattern='[0-9]+'></td>";
		echo "<td><input name='clichefull$clicheid' style='width:4ex;' value='".$cliche["full"]."'></td>";
		if ($cliche["doublepump"] == 1)
			$checked = " checked";
		else
			$checked = "";
		echo "<td><input type='checkbox' id='doublepump$clicheid' name='doublepump$clicheid'$checked><label for='doublepump$clicheid'> Double-pump cliché</label></td>";
		echo "<td><input type='checkbox' name='clichedelete".$cliche["clicheid"]."'> Delete</td>";
		echo "</tr>\n";
	}
	?>
	</table>
	<p>
	Add new:<br>
	<input name='newcliche' list="datalist_cliches">
	<input name='newclichecurrent' style='width:4ex;'>
	<input name='newclichefull' style='width:4ex;'><br>
	</p>
	</div>

	<p>
	<input type="submit" name="btnSubmit" value="Save">
	</p>
	</form>

<?php
}
require("inc_foot.php");
?>

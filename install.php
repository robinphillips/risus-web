<!DOCTYPE html>
<html lang="en">
<head>
<!--
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="risus.css" type="text/css" media="screen">

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<title>INSTALL [Risus Web]</title>
<script>
$(function() {
	$("#gmform").submit(function (evt) {
		msg = ""
		
		if ($("#password").val().length < 8) {
			if (msg != "")
				msg += "<br>"
			msg = "The password must be at least eight characters long"
		}
		if ($("#password").val() != $("#password2").val()) {
			if (msg != "")
				msg += "<br>"
			msg += "The passwords do not match"
		}
		
		if (msg != "") {
			// Show message and prevent form submission
			$("#msg").html(msg).show()
			evt.preventDefault()
		}
	})
})
</script>
</head>
<body>

<?php
require ("inc_config.php");
$db = new SQLite3(DBFILE);
?>

<h1>Install</h1>

<p id="msg" class="bad hidden;"></p>

<?php
function createtable ($db, $sql, &$bSuccess) {
	if ($db->exec($sql) == True)
		echo " <span class='good'>Success!</span>";
	else {
		echo " <span class='bad'>Failed!</span>";
		$bSuccess = False;
	}
	echo "<br>\n";
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$bSuccess = True;
	echo "<p>\n";
	
	echo "Database: creating 'characters' table.";
	$sql = "CREATE TABLE 'characters' ('charid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'npc' BOOLEAN DEFAULT 1, 'name' TEXT, 'description' TEXT, 'notes' TEXT, 'active' BOOLEAN DEFAULT 1 )";
	createtable ($db, $sql, $bSuccess);

	echo "Database: creating 'cliches' table.";
	$sql = "CREATE TABLE 'cliches' ( 'clicheid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'cliche_charid' INTEGER, 'cliche' TEXT, 'full' INTEGER, 'current' INTEGER , 'doublepump' BOOLEAN DEFAULT 0, 'loanboost' TEXT DEFAULT NULL)";
	createtable ($db, $sql, $bSuccess);

	echo "Database: creating 'log' table.";
	$sql = "CREATE TABLE 'log' ('logid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'datetime' DATETIME DEFAULT CURRENT_TIMESTAMP, 'log' TEXT)";
	createtable ($db, $sql, $bSuccess);

	echo "Database: creating 'players' table.";
	$sql = "CREATE TABLE 'players' ('playerid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'name' TEXT, 'player_charid' INTEGER DEFAULT 0, 'password' TEXT, 'gm' BOOLEAN DEFAULT 0, 'email' TEXT, 'reset' TEXT DEFAULT NULL, 'token' TEXT DEFAULT NULL)";
	createtable ($db, $sql, $bSuccess);
	
	echo "Database: creating GM user.";
	$sql = "INSERT INTO players (name, password, gm, email)
		VALUES (
		'".$db->escapeString($_POST["name"])."',
		'".$db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT))."',
		1,
		'".$db->escapeString($_POST["email"])."')";
	if ($db->exec($sql))
		echo " <span class='good'>Success!</span>";
	else {
		echo " <span class='bad'>Failed!</span>";
		$bSuccess = False;
	}
	echo "</p>\n";
	
	if ($bSuccess)
		echo "<p class='good'>Database created successfully.</p>\n";
	else
		echo "<p class='bad'>Errors creating database.</p>\n";
}
else {
?>
	<p>
	To install Risus Web, edit the <tt>inc_config.php</tt> file as required. Then fill in the details below and click the <b>Install</b> button.
	</p>

	<p>
	<form action="install.php" method="post" id="gmform">
	GM Name: <input name="name" required id="name"><br>
	GM Email: <input name="email" required id="email" type="email"><br>
	GM Password: <input type="password" name="password" id="password"><br>
	Confirm GM password: <input type="password" name="password2" id="password2">
	</p>
	<p>
	<input name="btnSubmit" type="submit" value="Install">
	</p>
	</form>
<?php
}
?>

<p>
<a href="index.php">Home</a>
</p>

</body>
</html>

<?php
$db->close();
?>

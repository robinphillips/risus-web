<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/
?>

<hr>
<p class="footerlinks">
:
<?php
foreach ($links as $url => $label) {
	// Pages named "gm_*" are GM-only - displayed italicised to GM
	if (substr($url, 0, 3) == "gm_")
		$class = " class='gmonlylink'";
	else
		$class = "";
	if (substr($url, 0, 3) != "gm_" || ROLE == "gm")
		echo "<a$class href='$url'>$label</a> : ";
}
echo "</p>\n";

if (count($extlinks) > 0 || (ROLE == "gm" && count($gm_extlinks) > 0)) {
	echo "<p class='footerlinks'>Extra links:\n";
	foreach ($extlinks as $url => $label)
		echo "<a href='$url' target='_blank'>$label</a> : ";
}
if (ROLE == "gm" && count($gm_extlinks) > 0) {
	foreach ($gm_extlinks as $url => $label)
		echo "<a class='gmonlylink' href='$url' target='_blank'>$label</a> : ";
}

if (count($extlinks) > 0 || (ROLE == "gm" && count($gm_extlinks) > 0))
	echo "</p>";

if (ROLE == "guest")
	echo "<p>You are viewing as a guest. Guests have limited access. <a href='".LOGINURL."'>Log in</a></p>";

if (ROLE == "gm" && file_exists("install.php"))
	echo "<p class='bad'>The <tt><a href='install.php'>install.php</a></tt> file exists. It should be deleted.</p>\n";
?>

<p class="footerlinks" style="float:right;">
<a href="about.php">Risus Web</a> by Robin Phillips, for GMs and players of <a href="https://www.drivethrurpg.com/product/170294/?affiliate_id=235519" target="_blank">Risus: The Anything RPG</a> by S. John Ross.
</p>
</body>
</html>

<?php
$db->close();
?>

<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/

--

Update a single cliché. GET parameters:
newvalue = new cliché value
clicheid = cliché ID to be updated
*/

$ajaxdb = new SQLite3(DBFILE);
if (intval($_GET["newvalue"]) == -1)
	$sql = "UPDATE cliches SET current = current-1 WHERE current > 0 AND clicheid = ".intval($_GET["clicheid"]);
else
	$sql = "UPDATE cliches SET current = ".intval($_GET["newvalue"])." WHERE clicheid = ".intval($_GET["clicheid"]);
$ajaxdb->exec($sql);

$ajaxdb->close();
?>

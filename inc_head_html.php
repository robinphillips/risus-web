<!DOCTYPE html>
<html lang="en">
<head>
<!--
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="risus.css" type="text/css" media="screen">

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<title>Risus Web</title>
<script>
$(function() {
	$("#linkselect").change(function(event) {
		if ($(this).find(':selected').data('type') == "internal")
			location.href = $(this).val()
		else
			window.open($(this).val(), '_blank')
	})
})
</script>
</head>
<body>

<?php
// Existing list of clichés
echo "\n<datalist id='datalist_cliches'>\n";
$sql = "SELECT cliche FROM cliches ORDER BY cliche";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC))
	echo "<option value='".htmlentities($cliche["cliche"], ENT_QUOTES)."'>\n";
echo "</datalist>\n";
?>

<span style="float:right;">
<select id="linkselect">
<?php
foreach ($links as $url => $label) {
	// Pages named "gm_*" are GM-only
	if (substr($url, 0, 3) == "gm_")
		$class = " class='gmonlylink'";
	else
		$class = "";
	if (substr($url, 0, 3) != "gm_" || ROLE == "gm") {
		echo "<option data-type='internal' value='$url'$class";
		if (($_SERVER['QUERY_STRING'] == "" && basename($_SERVER['PHP_SELF']) == $url) ||
			basename($_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']) == $url) {
				echo " selected";
				$title = $label;
		}
		echo ">$label</option>";
	}
}

if (count($extlinks)>0) {
	echo "<optgroup label='Extra Links'>";
	foreach ($extlinks as $url => $label)
		echo "<option data-type='external' value='$url'>$label</option>";
	if (ROLE == "gm" && count($gm_extlinks)>0)
		foreach ($gm_extlinks as $url => $label)
			echo "<option data-type='external' class='gmonlylink' value='$url'>$label</option>";
	echo "</optgroup>";
}
?>
</select>
</span>

<script>
$(function() {
<?php
// Disable submit buttons for guests, except on login page
if (ROLE == "guest" && basename($_SERVER['PHP_SELF']) != "login.php")
	echo "$('input[type=\"submit\"]').prop('disabled', true)\n";
?>
	document.title = "<?=htmlentities($title, ENT_QUOTES);?> [Risus Web]"
})
</script>

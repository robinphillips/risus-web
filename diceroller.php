<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	setcookie("rw_drnumber", intval($_POST["number"]), time()+60*60*24*30);
	setcookie("rw_drsides", intval($_POST["sides"]), time()+60*60*24*30);
}
require("inc_head_php.php");
require("inc_head_html.php");

// Get defaults
if (isset ($_POST["number"]))
	$drnumber = intval($_POST["number"]);
elseif (isset ($_COOKIE["rw_drnumber"]))
	$drnumber = intval($_COOKIE["rw_drnumber"]);
else
	$drnumber = 1;
if (isset ($_POST["number"]))
	$drsides = intval($_POST["sides"]);
elseif (isset ($_COOKIE["rw_drnumber"]))
	$drsides = intval($_COOKIE["rw_drsides"]);
else
	$drsides = 6;
?>

<script>
$(function() {
	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#results").hide()
	})
})
</script>

<h1>Dice Roller</h1>

<p>
Roll the selected number of dice. Individual values and total will be displayed.
</p>

<form method="post">
<input name="number" class="small" value="<?=$drnumber;?>" required pattern="[0-9]+">
<select name="sides">
<?php
$dice=array(4, 6, "Average", 8, 10, 12, 20, 100);
foreach ($dice as $die) {
	echo "<option value='$die'";
	if ($die == $drsides)
		echo " selected";
	echo ">D$die</option>";
}
?>
</select>
<input type="submit" value="Roll" name="btnSubmit" id="btnSubmit">
</form>

<?php
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Roll the dice
	$roll = dice_roll(intval($_POST["number"]), intval($_POST["sides"]));
	$log .= "<p>".PLAYERNAME." rolling ".intval($_POST["number"])." D".htmlentities($_POST["sides"], ENT_QUOTES).":<br>";
	foreach ($roll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . "<br>";
	// Total
	$log .= "Total: " . array_sum($roll) . "</p>";

	// Log the result
	logdb ($log);

	// Show results
	echo "<div class='box' id='results'><h2>Results</h2>\n";
	echo "$log</div>";
}

require("inc_foot.php");
?>

<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

if (isset($_GET["type"]) && $_GET["type"] == "sac")
	$type = "sac";
else
	$type = "combat";
require("inc_head_php.php");
require("inc_head_html.php");

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$htmlattacker = htmlentities($db->querySingle("SELECT name FROM characters WHERE charid = ".$_POST["attacker"]),ENT_QUOTES);
	$htmldefender = htmlentities($db->querySingle("SELECT name FROM characters WHERE charid = ".$_POST["defender"]),ENT_QUOTES);
	$attackcliche = $db->querySingle("SELECT * FROM cliches WHERE clicheid = ".$_POST["attackcliche"], True);
	$defendcliche = $db->querySingle("SELECT * FROM cliches WHERE clicheid = ".$_POST["defendcliche"], True);
	$updateclichesql = "";
	
	$log = "<p>$htmlattacker (".htmlentities($attackcliche["cliche"],ENT_QUOTES)." ".$attackcliche["current"].")";
	if ($type == "sac")
		$log .= " vs ";
	else
		$log .= " attacking ";
	$log .= "$htmldefender (".htmlentities($defendcliche["cliche"],ENT_QUOTES)." ".$defendcliche["current"].")<br>";

	$attackdice = $attackcliche["current"];
	// Inappropriate Cliché
	if (isset($_POST["inappropriateattack"]))
		$log .= "$htmlattacker is using an Inappropriate Cliché<br>";
	// Lucky Shot
	if (isset($_POST["luckyattack"])) {
		$attackdice++;
		$log .= "$htmlattacker is using a Lucky Shot<br>";
	}
	// Pump
	if (intval($_POST["pumpattack"]) > 0) {
		$pumpattack = intval($_POST["pumpattack"]);
		$attackdice += $pumpattack;
		$log .= "$htmlattacker is pumping by $pumpattack ".die_dice($pumpattack)."<br>";
		// Reduce attacker's cliché due to pump
		if ($attackcliche["doublepump"] == 1)
			$reduction = round($pumpattack / 2);
		else
			$reduction = $pumpattack;
		$new = $attackcliche["current"] - $reduction;
		$log .= "$htmlattacker's ".htmlentities($attackcliche["cliche"],ENT_QUOTES)." cliché is reduced by $reduction<br>";
		$updateclichesql .= "UPDATE cliches SET current = $new WHERE clicheid = ".$attackcliche["clicheid"].";\n";
		$db->exec($sql);
	}
	
	$defencedice = $defendcliche["current"];
	// Inappropriate Cliché
	if (isset($_POST["inappropriatedefence"]))
		$log .= "$htmldefender is using an Inappropriate Cliché<br>";
	// Lucky Shot
	if (isset($_POST["luckydefence"])) {
		$defencedice++;
		$log .= "$htmldefender is using a Lucky Shot<br>";
	}
	// Pump
	if (intval($_POST["pumpdefence"]) > 0) {
		$pumpdefence = intval($_POST["pumpdefence"]);
		$defencedice += $pumpdefence;
		$log .= "$htmldefender is pumping by $pumpdefence ".die_dice($pumpdefence)."<br>";
		// Reduce defender's cliché due to pump
		if ($defendcliche["doublepump"] == 1)
			$new = $defendcliche["current"] - round($pumpdefence / 2);
		else
			$new = $defendcliche["current"] - $pumpdefence;
		$log .= "$htmldefender's ".htmlentities($defendcliche["cliche"],ENT_QUOTES)." cliché is reduced to $new<br>";
		$updateclichesql .= "UPDATE cliches SET current = $new WHERE clicheid = ".$defendcliche["clicheid"].";\n";
		$db->exec($sql);
	}
	$log .= "</p>";
	
	// Roll the dice
	$attackroll = dice_roll($attackdice);
	$defendroll = dice_roll($defencedice);

	$log .= "<p>$htmlattacker rolls <strong>" . array_sum($attackroll) . "</strong> (";
	foreach ($attackroll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . ")<br>";
	$log .= "$htmldefender rolls <strong>" . array_sum($defendroll) . "</strong> (";
	foreach ($defendroll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . ")<br></p>";

	// Work out results
	$log .= "<p>";
	if ($type == "sac")
		$log .= "<b>";
	if (array_sum($attackroll) == array_sum($defendroll))
		$log .= "Draw!";
	elseif (array_sum($attackroll) > array_sum($defendroll)) {
		// Attacker won
		$log .= "$htmlattacker wins.";
		if ($type != "sac") {
			$log .= "<br>$htmldefender loses ";
			if (isset($_POST["inappropriateattack"]) && !isset($_POST["inappropriatedefence"])) {
				$damage = 3;
				$log .= "<i>three points</i>";
			}
			else {
				$damage = 1;
				$log .= "one point";
			}
			$log .= " from ".htmlentities($defendcliche["cliche"],ENT_QUOTES);
			$updateclichesql .= "UPDATE cliches SET current = current-$damage WHERE clicheid = ".$_POST["defendcliche"].";\n";
		}
	}
	else {
		// Defender won
		$log .= "$htmldefender wins.";
		if ($type != "sac") {
			$log .= "<br>$htmlattacker loses ";
			if (isset($_POST["inappropriatedefence"]) && !isset($_POST["inappropriateattack"])) {
				$damage = 3;
				$log .= "<i>three points</i> (inappropriate cliché)";
			}
			else {
				$damage = 1;
				$log .= "one point";
			}
			$log .= " from ".htmlentities($attackcliche["cliche"],ENT_QUOTES);
			$updateclichesql .= "UPDATE cliches SET current = current-$damage WHERE clicheid = ".$_POST["attackcliche"].";\n";
		}
	}
	if ($type == "sac")
		$log .= "</b>";
	$log .= "</p>";
	// Update clichés
	$db->exec($updateclichesql);
	// Get new values
	$sql = "SELECT current FROM cliches WHERE clicheid = ".$_POST["defendcliche"];
	$defendnew = $db->querySingle($sql);
	$sql = "SELECT current FROM cliches WHERE clicheid = ".$_POST["attackcliche"];
	$attacknew = $db->querySingle($sql);
	$log .= "<p>$htmlattacker's ".htmlentities($attackcliche["cliche"],ENT_QUOTES)." cliché is now $attacknew<br>";
	$log .= "$htmldefender's ".htmlentities($defendcliche["cliche"],ENT_QUOTES)." cliché is now $defendnew<br>";
	if ($defendnew <= 0)
		$log .= "<b>$htmldefender is defeated!</b><br>";
	if ($attacknew <= 0)
		$log .= "<b>$htmlattacker is defeated!</b><br>";
	$log .= "</p>";

	// Log the results
	logdb ($log);
}

if ($type == "sac")
	echo "<h1>Single-Action Conflict</h1>";
else
	echo "<h1>Combat</h1>";
?>
<script>
$(function() {
	// Update cliché lists on load
	$('#attackcliche').load('./ajax_clicheoptionlist.php?charid='+$("#attacker").val())
	$('#defendcliche').load('./ajax_clicheoptionlist.php?charid='+$("#defender").val())

	// Update cliché lists when attacker or defender changes
	$("#attacker").change(function(event){
		$('#attackcliche').load('./ajax_clicheoptionlist.php?charid='+$("#attacker").val())
	})
	$("#defender").change(function(event){
		$('#defendcliche').load('./ajax_clicheoptionlist.php?charid='+$("#defender").val())
	})

	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#results").hide()
	})
})
</script>

<form method="post">
<div class="box">
<p class="boxtitle">Attacker</p>
<p>
<select name="attacker" id="attacker">
<?php
if (!isset($_POST["attacker"])) // Attacker is not set. Default to player's character
	$selectedattacker = selectCharacters($db, CHARACTERID);
else
	$selectedattacker = selectCharacters($db, $_POST["attacker"]);
?>
</select>
<select name="attackcliche" id="attackcliche">
<?php
$sql = "SELECT * FROM cliches WHERE cliche_charid = $selectedattacker ORDER BY full DESC";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".$cliche["clicheid"]."'";
	if (isset ($_POST["attackcliche"]) && $cliche["clicheid"] == intval($_POST["attackcliche"]))
		echo " selected";
	echo ">".$cliche["cliche"]." ".$cliche["current"]." ".clichevalue($cliche["full"], $cliche["doublepump"])."</option>";
}
?>
</select>
</p>
<?php
if ($type != "sac") {
	if (isset($_POST["inappropriateattack"]))
		$check = " checked";
	else
		$check = "";
	?>
	<p class="inappropriate">
	<input name="inappropriateattack" type="checkbox" id="inappropriateattack"<?=$check;?>> <label for="inappropriateattack">This is an Inappropriate Cliché</label>
	</p>
<?php
}
?>
<p>
<label for="pumpattack">Pump: </label>
<select name="pumpattack" id="pumpattack">
<option value="0">Not pumping</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
<input name="luckyattack" type="checkbox" id="luckyattack"> <label for="luckyattack">Use a Lucky Shot</label>
</p>
</div>

<div class="box">
<p class="boxtitle">Defender</p>
<p>
<select name="defender" id="defender">
<?php
if (isset($_POST["defender"]))
	$selecteddefender = selectCharacters($db, $_POST["defender"]);
else
	$selecteddefender = selectCharacters($db, 0);
?>
</select>
<select name="defendcliche" id="defendcliche">
<?php
$sql = "SELECT * FROM cliches WHERE cliche_charid = $selecteddefender ORDER BY full DESC";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".$cliche["clicheid"]."'";
	if (isset ($_POST["defendcliche"]) && $cliche["clicheid"] == intval($_POST["defendcliche"]))
		echo " selected";
	echo ">".$cliche["cliche"]." ".$cliche["current"]." ".clichevalue($cliche["full"], $cliche["doublepump"])."</option>";
}
?>
</select>
</p>
<?php
	if ($type != "sac") {
	if (isset($_POST["inappropriatedefence"]))
		$check = " checked";
	else
		$check = "";
	?>
	<p class="inappropriate">
	<input name="inappropriatedefence" type="checkbox" id="inappropriatedefence"<?=$check;?>> <label for="inappropriatedefence">This is an Inappropriate Cliché</label>
	</p>
<?php
}
?>
<p>
<label for="pumpdefence">Pump: </label>
<select name="pumpdefence" id="pumpdefence">
<option value="0">Not pumping</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
<input name="luckydefence" type="checkbox" id="luckydefence"> <label for="luckydefence">Use a Lucky Shot</label>
</p>
</div>

<p>
<input type="submit" name="btnSubmit" value="Roll the Dice" id="btnSubmit">
</p>
</form>

<?php
if ($log != "") {
	echo "<div class='box' id='results'><h2>Results</h2>\n";
	echo "<p>$log</p></div>\n";
}

require("inc_foot.php");
?>
